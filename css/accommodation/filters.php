
<style>
@media (min-width: 992px) {
	#brandchap #wrapper #accommodation .filters {
    position: relative;
    width: auto;
    overflow: inherit;
	}
	#brandchap #wrapper #accommodation .gmap,
	#brandchap #wrapper #accommodation .sub_cat {
    margin-left: 0;
    width: 100%;
	}
}
#wrapper #accommodation .filters {
	padding: 0;
	font-size: .9em;
}
.override #wrapper .filters a.btn {
    background: #f5f5f5;
}
.override #wrapper .accom .filters a.btn:hover {
    background: #ffffff;
}
.override #wrapper .filters a.btn.active:hover {
    background: #bababa;
		box-shadow: inset 0 1px 2px rgba(0,0,0,0.2);
}
.filters .flex_contain {
  display: flex;
  flex-wrap: wrap;
	margin: 0;
}
.main_options.flex_contain {
    flex: 1;
}
.filters .flex_contain.yourstay {
	flex: 2 1 65ch;
}
.filters .flex_contain.buttons,
.filters .flex_contain.submitting {
	flex: 1 1 50ch;
}
.flex_contain span {
    flex: 1 1 10ch;
    margin: .25rem;
    white-space: nowrap;
}
.flex_contain.yourstay span {
	flex: 1 1 22ch;
}
#yourstay input,
#yourstay select,
#wrapper #yourstay .btn,
#wrapper #filter_buttons .btn {
  width: 100%;
  padding: 0.5em 0.75em;
  font-size: .9em;
  line-height: 1.3;
	font-family: gill-sans-nova, Gill Sans, Gill Sans MT, "Lato", Calibri, sans-serif;
}
#wrapper #yourstay .btn.cta {
	background-color: #f99f1c;
	border-color: #f99f1c;
	color: #fff;
	font-weight: bold;
	letter-spacing: .1em;
	text-transform: uppercase;
}
#yourstay span.apply {
  flex: 3 1 20ch;
}
#yourstay span.reset {
  flex: 2 1 10ch;
}
#yourstay div > span.reset .btn {
  background-color: #f7f7f7;
  border-color: #d6d6d6;
  color: #aaaaaa;
}

.override .accom fieldset {
    border: 0;
}
.accom legend {
    display: none;
}
.accom .lightbox {
  background: #e4e4e4;
	background: linear-gradient(0deg, rgb(217 217 217) 0%, rgb(236 236 236) 100%);
  padding: 1rem;
  width: 100%;
	text-align: left;
  position: relative;
}
.accom .lightbox > div {
  display: flex;
  flex-flow: row wrap;
}
.accom .lightbox::before {
  content: ' ';
  position: absolute;
  top: 0;
  left: 50%;
	width: 0;
	height: 0;
	transform: translateX(-50%);
	border-style: solid;
	border-width: .75em .75em 0 .75em;
	border-color: #d6d6d6 transparent transparent transparent;
}
fieldset {
  flex: 16em;
}
.tag_choice {
	display: inline-block;
	width: 100%;
	padding: .6em .5em .6em .15em;
	border-bottom: 1px dotted #dbdbdb;
}
.tag_choice input {
	margin-right: .5em;
}
.tag_choice:last-child {
  border-bottom: 0;
}
.tag_choice .count {
	float: right;
}
#wrapper .filters .btn {
    font-size: 0.85rem;
    font-weight: 400;
    padding: 0.4rem 0.6rem;
    background: #fff;
    border-color: #4d4d4d;
    color: #4d4d4d;
}
#wrapper .accom .filters a.btn.active {
    background: #bababa;
}
.accom .filters svg {
    width: 1em;
    height: 1em;
    margin: 0 .5em 0 0;
}
.accom #filter_lightboxs span,
.accom #filters_lightbox span label,
.accom #filters_lightbox span > div {
	display: inline-block;
}
.accom #filters_lightbox span,
.accom #sort_lightbox span {
	width: 100%;
  flex: 20em;
  padding: 1.5em 1em;
}
.accom #sort_lightbox span select {
	width: 100%;
}

fieldset#filters_lightbox {
	padding: 3rem 2rem 0rem;
}
#location_lightbox ul,
#location_lightbox ul ul {
  padding: 0;
}
#location_lightbox div > ul > li {
  width: 100%;
	background: rgb(0 0 0 / 10%);
}
#location_lightbox div > ul > li label {
  padding: 0 0 0 .5em;
  width: calc(100% - .5em);
  display: block;
}

#location_lightbox div > ul > li label:hover {
	cursor: pointer;
}
#location_lightbox ul li {
  display: inline-block;
  font-size: 1rem;
  width: 17em;
  background: rgb(255 255 255 / 30%);
  margin: .5em;
  border-radius: 0.2rem;
  position: relative;
  padding: .5em;
  z-index: 0;
}
#location_lightbox input {
  opacity: 0;
  width: 3em;
  right: 0;
  top: 0;
  bottom: 0;
  height: 2em;
  position: absolute;
  z-index: 2;
}
#location_lightbox input:hover {
	cursor: pointer;
}
#location_lightbox .slider {
  display: inline-block;
  cursor: pointer;
  height: 1em;
  width: 2em;
  right: .75em;
  top: .75em;
  background-color: rgba(0,0,0,.1);
  -webkit-transition: .4s;
  transition: .4s;
  position: absolute;
  border-radius: 0.2rem;
  z-index: 1;
}

#location_lightbox .slider:before {
  position: absolute;
  content: " ";
  height: .8em;
  width: .8em;
  left: .1em;
  bottom: .1em;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
  border-radius: 0.2rem;
}

#location_lightbox input:checked + .slider {
  background-color: #f99f1c;
}

#location_lightbox input:focus + .slider {
  border: 0 0 1px #f99f1c;
}

#location_lightbox input:checked + .slider:before {
  -webkit-transform: translateX(1em);
  -ms-transform: translateX(1em);
  transform: translateX(1em);
}
.accom #filters_lightbox span:not:last-of-type {
	border-bottom: 1px dotted #eee;
}
.accom #filters_lightbox span label {
	width: 5rem;
  text-align: right;
  padding-right: 1.5em;
  font-weight: bold;
}
.accom #filters_lightbox span > div {
	width: calc(100% - 6rem);
}
.accom #filters_lightbox span.desciption.small {
    width: 100%;
    display: block;
    padding: 0 0 1rem 5rem;
    text-align: center;
		margin: 0;
}
.accom .alert {
    font-size: 1rem;
    padding: .5em;
    display: block;
    width: 100%;
    text-align: center;
		background: #f99f1c;
    border: 1px solid #f99f1c;
		margin-right: 2em;
}
.alert p {
    color: #ffffff;
}
#yourstay {
	padding: .5em;
	margin: 0;
  width: 100%;
	background: #d6d6d6;
}
#yourstay legend,
#yourstay label {
	display: none;
}
#accom_extras_lightbox {
	padding: 2rem 1rem;
	font-size: .7rem;
}
#accom_extras_lightbox fieldset {
  background: #ffffff;
  border-radius: .3em;
  margin: 1.5em 1em;
  position: relative;
  padding: 1em 1em .5em;
}
#accom_extras_lightbox legend {
	display: block;
  font-size: 1.2em;
  background: #ffffff;
  position: absolute;
  left: 0;
  top: -1.8em;
  padding: .2em 1em;
  border-radius: .2em .2em 0 0;
  white-space: nowrap;
}
#accom_extras_lightbox legend span {
  display: inline-block;
  vertical-align: middle;
	padding-right: .2em;
}
#accom_extras_lightbox legend img {
  width: auto;
  height: 1.5em;
}

.avdate {
  position: absolute;
  top: .5rem;
  left: .5rem;
}
.avdate > div {
	display: inline-block;
	vertical-align: top;
  background: rgba(255,255,255,0.8);
  border-radius: 0.25em;
  z-index: 20;
  text-align: center;
  width: 3.8em;
  font-size: .45em;
	padding: .5em 0 0;
  overflow: hidden;
	margin-right: .25em;
}
.avdate > div:nth-child(3) .day,
.avdate > div:nth-child(3) .month {
    font-weight: bold;
}
.gmap .avdate {
  top: 16px;
  left: 16px;
}
.avdate .day,
.avdate .month,
.avdate .status  {
  margin: 0;
  padding: 0;
  text-transform: uppercase;
  width: 100%;
  display: block;
  line-height: 1;
}
.avdate .day {
	font-size: 2.2em;
  margin-bottom: .04em;
}
.avdate .month {
    font-size: 1em;
}
.avdate .status {
  font-weight: bold;
  font-size: .5em;
  padding: .7em .5em;
  color: #ffffffe6;
  background: #333333e6;
  margin-top: 1em;
}

.avdate > div.Available {
  color: #7fd1b9;
}
.avdate >div.Available .status {
  background: #7fd1b9e6;
}

.avdate > div.Booked {
	color: #db5461;
}
.avdate > div.Booked .status {
  background: #db5461e6;
}

.avdate > div.Available.false,
.avdate > div.false {
  color: #f99f1c;
}
.avdate > div.false .status {
  background: #f99f1ce6;
}
.avdate > div,
.price {
	backdrop-filter: blur(3px);
}
.accom p.small.price {
	top: .5em;
	right: .5em;
  width: 3.5em;
  line-height: .7;
  text-align: center;
}
p.small.price span {
    width: 100%;
    display: inline-block;
}
p.small.price span.from {
  font-size: .55em;
  padding-bottom: .5em;
}
p.small.price span.cost {
  font-weight: bold;
}
 p.small.price span.cost::first-letter {
	 font-weight: 300;
 }
p.small.price span.time {
  font-size: .45em;
  text-transform: uppercase;
  padding: .3em 0 .8em;
}
</style>

<?php $cat_title = single_cat_title('', false); ?>


<nav class="accom filters dark">

<?php

// GETTING ALL THE TAGS AND META FIELDS
$term_args = array(
	'post_type' => 'accom',
	'posts_per_page' => '-1',
	'include_children'	=> 1
);
$filterquery = new WP_Query( $term_args );
if( $filterquery->have_posts() ) {

	while ( $filterquery->have_posts() ) : $filterquery->the_post();

	// vars
	$max_occupancy = get_field('max_occupancy');
	$max_price = get_field('max-price');
	$min_price = get_field('min-price');
	$no_rooms = get_field('no_rooms');

	if( !isset($features['max-price']) || ( isset($features['max-price']) && ( intval($max_price) > intval($features['max-price']) ) ) ) {
		$features['max-price'] = intval($max_price);
	}

	if( !isset($features['min-price']) || ( isset($features['min-price']) && ( intval($min_price) < intval($features['min-price']) ) ) ) {
		$features['min-price'] = intval($min_price);
	}

	if( !isset($features['max_occupancy']) || ( isset($features['max_occupancy']) && ( intval($max_occupancy) > intval($features['max_occupancy']) ) ) ) {
		$features['max_occupancy'] = intval($max_occupancy);
	}

	if( !isset($features['no_rooms']) || ( isset($features['no_rooms']) && ( intval($no_rooms) > intval($features['no_rooms']) ) ) ) {
		$features['no_rooms'] = intval($no_rooms);
	}

	endwhile;

}


	$taxonomy     = 'accommodation_type'; ?>


	<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter">

		<fieldset id="yourstay">

			<legend><label for="location_filter">Your stay</legend>

				<div class="flex_contain main_options">
					<div class="flex_contain yourstay">

						<span class="arrivaldate">
							<label for="arrival-date">Arival date</label>
							<input type="date" id="arrival-date" name="arrival-date" <?php if( isset($filters['arrival']) && $filters['arrival'] !=='' ) { echo "value='".$filters['arrival']."'"; }; ?>>
						</span>


						<span class="location_btn">
							<a id="location_btn" onclick="showHideExtras(this.id)" class="btn"><svg class="icon icon-location"><use xlink:href="#icon-location"></use></svg><span>Location</span></a>
						</span>

					</div>




				<div class="flex_contain buttons">


					<span class="filters_btn">
						<a id="filters_btn" onclick="showHideExtras(this.id)" class="btn"><svg class="icon icon-equalizer"><use xlink:href="#icon-equalizer"></use></svg> <span>Price + Guests</span></a>
					</span>



					<span class="sort_btn">
						<a id="sort_btn" onclick="showHideExtras(this.id)" class="btn"><svg class="icon icon-sort-alpha-asc"><use xlink:href="#icon-sort-alpha-asc"></use></svg><span>View options</span></a>
					</span>



					<span class="accom_extras_btn">
						<a id="accom_extras_btn" onclick="showHideExtras(this.id)" class="btn"><svg class="icon icon-checkbox-checked"><use xlink:href="#icon-checkbox-checked"></use></svg><span>Extras</span></a>
					</span>


				</div>



				<div class="flex_contain submitting">
					<span class="reset">
						<input class='btn' type="reset" value="Clear">
					</span>
					<span class="apply">
						<button class='btn cta'>Apply filters</button>
					</span>
				</div>



			</div>



			<script> function showHideExtras(clicked_id) {

				var locations_lightbox = document.getElementById("location_lightbox");
				var sort_lightbox = document.getElementById("sort_lightbox");
				var accom_extras_lightbox = document.getElementById("accom_extras_lightbox");
				var filters_lightbox = document.getElementById("filters_lightbox");

				var locations_btn = document.getElementById("location_btn");
				var sort_btn = document.getElementById("sort_btn");
				var accom_extras_btn = document.getElementById("accom_extras_btn");
				var filters_btn = document.getElementById("filters_btn");

				var clickedBtnID = document.getElementById(clicked_id);

				var lightboxToOpen = clicked_id.replace("btn", "lightbox");
				var lightboxToOpenID = document.getElementById(lightboxToOpen);

				var CurrentClickedLightboxState = lightboxToOpenID.style.display;


				if ( CurrentClickedLightboxState == 'block' ) {

					sort_lightbox.style.display = "none";
					locations_lightbox.style.display = "none";
					accom_extras_lightbox.style.display = "none";
					filters_lightbox.style.display = "none";

				}

				if ( CurrentClickedLightboxState == 'none' ) {

				  sort_lightbox.style.display = "none";
				  locations_lightbox.style.display = "none";
				  accom_extras_lightbox.style.display = "none";
				  filters_lightbox.style.display = "none";

					lightboxToOpenID.style.display = 'block';

				}


				// Check if button clicked was active: if not, make it active. If it is, remove active class.
				if (sort_btn != clickedBtnID) {
					sort_btn.classList.remove('active');
				}
				if (locations_btn != clickedBtnID) {
					locations_btn.classList.remove('active');
				}
				if (accom_extras_btn != clickedBtnID) {
					accom_extras_btn.classList.remove('active');
				}
				if (filters_btn != clickedBtnID) {
					filters_btn.classList.remove('active');
				}

				if (clickedBtnID.classList) {
				  clickedBtnID.classList.toggle("active");
				} else {
				  // For IE9
				  var classes = clickedBtnID.className.split(" ");
				  var i = classes.indexOf("active");

				  if (i >= 0)
				    classes.splice(i, 1);
				  else
				    classes.push("active");
				    clickedBtnID.className = classes.join(" ");
				}

			} </script>

		</fieldset>

		<fieldset id="location_lightbox" class="lightbox" style="display: none;">
			<legend>Choose locations</legend>

			<div>

				<?php // Get all the categories in hierarchical order
				$hiterms = get_terms($taxonomy, array("orderby" => "slug", "parent" => 0));
				echo '<ul>';

				foreach($hiterms as $key => $hiterm) {

					echo '<li>';
					echo '<label for="'.$hiterm->slug.'" class="cat_choice all"><input type="checkbox"';
					if (isset($locationArray) && in_array($hiterm->slug, $locationArray)) { echo " checked "; };
					echo 'id="'.$hiterm->slug.'" name="location '.$hiterm->slug.'" value="'.$hiterm->slug.'">'.$hiterm->name.'<span class="slider"></span></label>';

					$loterms = get_terms($taxonomy, array("orderby" => "slug", "parent" => $hiterm->term_id));

					if($loterms) {
						echo '<ul>';
						foreach($loterms as $key => $loterm) {

							echo '<li>';
							if ( $loterm->name == $cat_title ) {
								echo '<label for="'.$loterm->slug.'" class="cat_choice child"><input type="checkbox"';
								if (isset($locationArray) && in_array($loterm->slug, $locationArray)) { echo " checked "; };
								echo 'id="'.$loterm->slug.'" name="location '.$loterm->slug.'" value="'.$loterm->slug.'">'.$loterm->name.'<span class="slider"></span></label>';
							} else {
								echo '<label for="'.$loterm->slug.'" class="cat_choice child"><input type="checkbox"';
								if (isset($locationArray) && in_array($loterm->slug, $locationArray)) { echo " checked "; };
								echo 'id="'.$loterm->slug.'" name="location '.$loterm->slug.'" value="'.$loterm->slug.'">'.$loterm->name.'<span class="slider"></span></label>';
							}
							echo '</li>';

							// Thrid level
							// $loerterms = get_terms($taxonomy, array("orderby" => "slug", "parent" => $loterm->term_id));
							//
							// if($loerterms) {
							// 	echo '<ul>';
							// 	foreach($loerterms as $key => $loerterm) {
							//
							// 		echo '<li>';
							//
							// 		if ( $loerterm->name == $cat_title ) {
							// 			echo '<div class="cat_choice child"><input type="checkbox"';
							// 			if (isset($locationArray) && in_array($loerterm->slug, $locationArray)) { echo " checked "; };
							// 			echo 'id="'.$loerterm->slug.'" name="location '.$loerterm->slug.'" value="'.$loerterm->slug.'"><label for="'.$loerterm->slug.'">'.$loerterm->name.'</label><span class="slider"></span></div>';
							// 		} else {
							// 			echo '<div class="cat_choice child"><input type="checkbox"';
							// 			if (isset($locationArray) && in_array($loerterm->slug, $locationArray)) { echo " checked "; };
							// 			echo 'id="'.$loerterm->slug.'" name="location '.$loerterm->slug.'" value="'.$loerterm->slug.'"><label for="'.$loerterm->slug.'">'.$loerterm->name.'</label><span class="slider"></span></div>';
							// 		}
							// 		echo '</li>';
							//
							// 	}
							// 	echo '</ul>';
							// }
						}
						echo '</ul>';
					}
					echo '</li>';
				}
				echo '</ul>'; ?>

			</div>
		</fieldset>




		<fieldset id="filters_lightbox" class="lightbox" style="display: none;">
			<legend>Filter</legend>
			<div>
				<!-- PRICE slider -->
				<span class="price">
					<label for="price-min">Price</label>
					<input type="hidden" min="<?php echo $features['min-price'] ?>" placeholder="Max price" max="<?php echo $features['max-price'] ?>" value="0" name="price-min" id="price-min">
					<input type="hidden" min="<?php echo $features['min-price'] ?>" placeholder="Min price" max="<?php echo $features['max-price'] ?>" value="<?php echo $features['max-price'] ?>" name="price-max" id="price-max">
					<div id='price-slider'></div>
					<span class="desciption small">Based on a 7 night stay</span>
					<?php
					if ($features['max-price'] > 1000 ) {
						$halfmaxprice = $features['max-price'] / 4;
						$steprange = $halfmaxprice / 10;
					} else {
							$halfmaxprice = $features['max-price'];
							$steprange = $halfmaxprice / 10;
					}
					$GLOBALS['footer-js'] .= "
					var priceSlider = document.getElementById('price-slider');
					noUiSlider.create(priceSlider, {
				    start: [".$features['min-price'].", ".$features['max-price']."],
				    connect: true,
						step: 1,
				    range: {
				        'min': [".$features['min-price'].", 5],
								'85%': [".$halfmaxprice.", ".$steprange."],
				        'max': [".$features['max-price']."]
				    },
						tooltips: [wNumb({decimals: 0, prefix: '£', thousand: ','}), wNumb({decimals: 0, prefix: '£', thousand: ','})],
						format: wNumb({
			        decimals: 0
				    })
					});";

					if ( isset($filters['min-price']) && $filters['min-price'] && isset($filters['max-price']) && $filters['max-price'] ) {
						$GLOBALS['footer-js'] .= "priceSlider.noUiSlider.set([ ".$filters['min-price'].", ".$filters['max-price']." ])";
					} else if ( isset($filters['max-price']) && $filters['max-price'] ) {
						$GLOBALS['footer-js'] .= "priceSlider.noUiSlider.set([ null , ".$filters['max-price']." ])";
					} else if ( isset($filters['min-price']) && $filters['min-price'] ) {
						$GLOBALS['footer-js'] .= "priceSlider.noUiSlider.set([ ".$filters['min-price'].", null ])";
					}

					$GLOBALS['footer-js'] .= "
					var inputPriceMax = document.getElementById('price-max');
					var inputPriceMin = document.getElementById('price-min');
					priceSlider.noUiSlider.on('update', function (values, handle) {
					    var value = values[handle];
					    if (handle) {
					        inputPriceMax.value = value;
					    } else {
					        inputPriceMin.value = value;
					    }
					});
					inputPriceMin.addEventListener('change', function () {
					    priceSlider.noUiSlider.set([this.value, null]);
					});
					inputPriceMax.addEventListener('change', function () {
					    priceSlider.noUiSlider.set([null, this.value]);
					});"?>
			</span>

			<!-- SLEEP slider -->
			<span class="sleeps">
				<label for="max_occupancy">Guests</label>
				<input type="hidden" min="1" max="<?php echo $features['max_occupancy'] ?>" value="1" name="max_occupancy" id="max_occupancy">
				<div id='max_occupancy_slider'></div>
				<span class="desciption small">Must have room for at least this many guests</span>
				<?php $GLOBALS['footer-js'] .= "
				var maxOccupancySlider = document.getElementById('max_occupancy_slider');
				noUiSlider.create(maxOccupancySlider, {
					start: [1],
					connect: [true, false],
					step: 1,
					range: {
						'min': 1,
						'max': ".$features['max_occupancy']."
					},
					tooltips: [wNumb({decimals: 0})],
					format: wNumb({
						decimals: 0
					})
				});";

				if ( isset($filters['max-occupancy']) && $filters['max-occupancy']  ) {
					$GLOBALS['footer-js'] .= "maxOccupancySlider.noUiSlider.set([ ".$filters['max-occupancy']." ]);";
				}

				$GLOBALS['footer-js'] .= "var maxOccupancyValueElement = document.getElementById('max_occupancy');
				maxOccupancySlider.noUiSlider.on('update', function (values, handle) {
				    maxOccupancyValueElement.value = values[handle];
				});
				maxOccupancyValueElement.addEventListener('change', function () {
						maxOccupancySlider.noUiSlider.set([this.value, null]);
				});"?>
			</span>


			<span class="rooms">
				<label for="room-min">Bedrooms</label>
				<input type="hidden" min="1" max="<?php echo $features['no_rooms'] ?>" value="1" name="room" id="room">
				<div id='room-slider'></div>
				<span class="desciption small">Minimum number of seperate bedrooms</span>
				<?php $GLOBALS['footer-js'] .= "
				var roomSlider = document.getElementById('room-slider');
				noUiSlider.create(roomSlider, {
						start: [1],
						connect: [true, false],
						step: 1,
						range: {
								'min': 1,
								'max': ".$features['no_rooms']."
						},
						tooltips: [wNumb({decimals: 0})],
						format: wNumb({
			        decimals: 0
				    })
				});";

				if ( isset($filters['min-rooms']) && $filters['min-rooms']  ) {
					$GLOBALS['footer-js'] .= "roomSlider.noUiSlider.set([ ".$filters['min-rooms']." ]);";
				}

				$GLOBALS['footer-js'] .= "var roomValueElement = document.getElementById('room');
				roomSlider.noUiSlider.on('update', function (values, handle) {
				    roomValueElement.value = values[handle];
				});"?>
			</span>
		</div>
	</fieldset>





	<fieldset id="sort_lightbox" class="lightbox" style="display: none;">
		<legend>View options</legend>
		<div>
			<span class="sortby">
				<label>Sort by
				<select name="sortby" id="sortby">
					<?php if( isset($filters['sortby']) && $filters['sortby'] !=='' ) { ?>
							<option value="title" <?php if($filters['sortby'] =='title') { echo "selected='selected'"; }; ?>/>Property Name</option>
							<option value="date" <?php if($filters['sortby'] =='date') { echo "selected='selected'"; }; ?>/>Date Added</option>
							<option value="price" <?php if($filters['sortby'] =='price') { echo "selected='selected'"; }; ?>/>Price</option>
							<option value="rand" <?php if($filters['sortby'] =='rand') { echo "selected='selected'"; }; ?> />Random</option>
					<?php } else { ?>
						<option value="title" />Property Name</option>
						<option value="date" />Date Added</option>
						<option value="price" />Price</option>
						<option value="rand" selected="selected" />Random</option>
					<?php } ?>
				</select></label>
			</span>
			<span class="sortorder">
				<label>Sort order
				<select name="sortorder" id="sortorder">
					<?php if( isset($filters['order']) && $filters['order'] !=='' ) { ?>
							<option value="ASC"  <?php if($filters['order'] =='ASC') { echo "selected='selected'"; }; ?> />↓ 0-9 / A-Z</option>
							<option value="DESC" <?php if($filters['order'] =='DESC') { echo "selected='selected'"; }; ?>/>↑ 9-0 / Z-A</option>
					<?php } else { ?>
						<option value="ASC"  selected="selected" />↓ 0-9 / A-Z</option>
						<option value="DESC" />↑ 9-0 / Z-A</option>
					<?php } ?>
				</select></label>
			</span>

			<span class="viewas">
				<label>View as</label>
				<select name="viewtype">
					<option value="grid"  <?php if ( ( isset($filters['viewtype']) && $filters['viewtype'] !== 'map' ) || !isset($filters['viewtype'] ) ) { echo 'selected="selected"'; }?> />Grid</option>
					<option value="map" <?php if ( isset($filters['viewtype']) && $filters['viewtype'] == 'map') { echo 'selected="selected"'; }?>/>Map</option>
				</select>
			</span>
		<div>
	</fieldset>



	<input type="hidden" name="action" value="myfilter">





	<div id="accom_extras_lightbox" class="lightbox" style="display: none;">
		<div>
	<?php
		// Get all tags
		$accom_tags = get_terms( 'accommodation_feature' );
		$i = '';
		if ($accom_tags) {
			foreach ( $accom_tags as $tag ) {
				$order = $tag->parent;
				$tag_ids[$tag->term_id] = $order . '_ko' . $i;
				++$i;
				unset($order);
			}
		}
		$orderPrev = '';
		asort($tag_ids);

		$tag_ids = str_replace('zMisc', 'Misc', $tag_ids );
		$tag_id_only = array_keys($tag_ids);


		foreach($tag_ids as $tag => $order) {

				$term = get_term( $tag );

				if ( count( get_term_children( $term->term_id, 'accommodation_feature' ) ) > 0 ) {

					echo '<fieldset id="'.$term->term_id.'"><div id="'.$term->term_id.'_toggle"></div><legend>';
					if ( get_field('af_ico', 'accommodation_feature' . '_' . $term->term_id) ) {
						$icon = get_field('af_ico', 'accommodation_feature' . '_' . $term->term_id);
						echo '<span><img class="icon" src="' . $icon['url'] . '"></span> ';
					}
					echo '<span>' . $term->name . '</span></legend>';
					$child_term = get_term_children( $term->term_id, 'accommodation_feature' );
					foreach( $child_term as $child_tag ) {
						$term = get_term( $child_tag );
						echo '<div class="tag_choice"><input type="checkbox"';
						if (isset($extrasArray) && in_array($term->slug, $extrasArray)) { echo " checked "; };
						echo 'id="'.$term->slug.'" name="tag '.$term->slug.'" value="'.$term->slug.'"><label for="'.$term->slug.'">'.$term->name.'</label></div>';
					}
					echo '</fieldset>';

				}

		}
			?>
		</div>
	</div>

</form>

<script>
jQuery(function($){

	$("#location_lightbox input#all").on('change',function() {
		if(!$(this).is(':checked')) {
			$('#location_lightbox .cat_choice.child input:checkbox').prop('checked',true);
		} else {
			$('#location_lightbox .cat_choice.child input:checkbox').prop('checked',false);
		}
	});

		$("#location_lightbox .cat_choice.child input:checkbox").on('change',function() {
			if($(this).is(':checked')) {
				$('#location_lightbox input#all').prop('checked',false);
			}
		});

	$('#filter').submit(function(){

		var filter = $('#filter');
		var filterArray = filter.serializeArray();

		var locations_lightbox = document.getElementById("location_lightbox");
		var sort_lightbox = document.getElementById("sort_lightbox");
		var accom_extras_lightbox = document.getElementById("accom_extras_lightbox");
		var filters_lightbox = document.getElementById("filters_lightbox");

		var locations_btn = document.getElementById("location_btn");
		var sort_btn = document.getElementById("sort_btn");
		var accom_extras_btn = document.getElementById("accom_extras_btn");
		var filters_btn = document.getElementById("filters_btn");

		location_lightbox.style.display = "none";
		sort_lightbox.style.display = "none";
		accom_extras_lightbox.style.display = "none";
		filters_lightbox.style.display = "none";

		sort_btn.classList.remove('active');
		locations_btn.classList.remove('active');
		accom_extras_btn.classList.remove('active');
		filters_btn.classList.remove('active');

		var URLend = '';
		var formData = {};

    $.each(filterArray, function(i, field) {
    	if(field.value.trim() != "") {
    		if(formData[field.name] != undefined){
    			var val = formData[field.name];
    			if(!Array.isArray(val)){
    				 arr = [val];
    			}
    			arr.push(field.value.trim());
    			formData[field.name] = arr;
    		}else{
    		  formData[field.name] = field.value;
    		}
      }
    });

		$.ajax({
			url:filter.attr('action'),
			data:filter.serialize(), // form data
			type:filter.attr('method'), // POST
			beforeSend:function(xhr){
				filter.find('button.cta').text('Finding properties...'); // changing the button label
			},
			success:function(data){
				console.log(formData);
				$(".cat_header").css('background-image', 'none');
				$('.cat_header .txt_blk').remove();
				$(".cat_header").html("<div class='content txt_blk avs_default normal'><h1 style='text-align: center;'>Lake District Self Catered Cottages</h1></div>");
				filter.find('button').text('Apply filter'); // changing the button label back
				$('#response').html(data); // insert data
			}
		});
		return false;
	});
});
</script>

</nav>
