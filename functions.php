<?php
add_filter( 'gform_form_tag', 'form_tag', 10, 2 );
function form_tag( $form_tag, $form ) {
if ( $form['id'] != 7 ) { // change 1 to match your form id
//not the form whose tag you want to change, return the unchanged tag
return $form_tag;
}
// Turn off autocompletion as described here https://developer.mozilla.org/en-US/docs/Web/Security/Securing_your_site/Turning_off_form_autocompletion
$form_tag = preg_replace( "|action='|", "autocomplete='off' action='", $form_tag );
return $form_tag;
}

// Set client colour pallet for use in colour picker
function my_acf_input_admin_footer() { ?>
<script type="text/javascript">
(function($) {

  acf.add_filter('color_picker_args', function( args, $field ) {
  	args.palettes = ['#F99F1C', '#57727B', '#531C3F', '#DB5461', '#7FD1B9']
  	return args;
  });

})(jQuery);
</script>
<?php

}

add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');
